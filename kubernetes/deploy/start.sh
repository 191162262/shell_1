#!/usr/bin/env bash
#
set -ex
# jvm
JAVA_HOME="/opt/java/jdk1.8.0_202"
MEM_ARGS="-Xms256m -Xmx256m"
GC_ARGS="-XX:+UseG1GC \
-XX:SurvivorRatio=8 \
-XX:MaxGCPauseMillis=400 \
-XX:G1ReservePercent=15 \
-XX:InitiatingHeapOccupancyPercent=45 \
-XX:+PrintGCDetails \
-XX:+PrintGCDateStamps \
-XX:+HeapDumpOnOutOfMemoryError \
-XX:HeapDumpPath=${APP_HOME}/logs/ \
-Xloggc:${APP_HOME}/logs/gc.log"
JMX_ARGS="-Dcom.sun.management.jmxremote \
-Dcom.sun.management.jmxremote.authenticate=false \
-Dcom.sun.management.jmxremote.ssl=false \
-Dcom.sun.management.jmxremote.port=18080"

# spring options
SPRING_OPTIONS="--server.port=8080 --spring.profiles.active=dev"

# jvm options
JAVA_OPTIONS="${MEM_ARGS} ${GC_ARGS} ${JMX_ARGS}"

# start jar
cd ${APP_HOME}/
java ${JAVA_OPTIONS} -jar start.jar ${SPRING_OPTIONS}
