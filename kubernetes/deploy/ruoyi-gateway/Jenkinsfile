pipeline {
  agent {
    node {
      label 'maven'
    }

  }
  stages {
    stage('clone code') {
      agent none
      steps {
        container('maven') {
          git(url: 'https://gitee.com/haroldzz/RuoYi-Cloud.git', credentialsId: 'gitee', branch: 'master', changelog: true, poll: false)
        }

      }
    }

    stage('unit test') {
      steps {
        container('maven') {
          sh 'mvn clean test'
        }

      }
    }

    stage('build & push') {
      agent none
      steps {
        container('maven') {
          sh 'mvn -Dmaven.test.skip=true clean package'
          sh 'cp ./ruoyi-gateway/target/ruoyi-gateway.jar start.jar'
          sh 'cp ./deploy/docker-entrypoint.sh ./'
          sh 'cp ./deploy/start.sh ./'
          sh 'docker build -f deploy/Dockerfile -t $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER .'
          withCredentials([usernamePassword(credentialsId: 'harbor', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
            sh 'echo "$DOCKER_PASSWORD" | docker login $REGISTRY -u "$DOCKER_USERNAME" --password-stdin'
            sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER'
          }

        }

      }
    }

    stage('push latest') {
      when {
        branch 'master'
      }
      steps {
        container('maven') {
          sh 'docker tag  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:latest '
          sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:latest '
        }

      }
    }

    stage('deploy to dev') {
      agent none
      steps {
        container('maven') {
          withCredentials([kubeconfigContent(credentialsId: 'k8s-1718281828', variable: 'KUBECONFIG_CONFIG')]) {
            sh 'mkdir -p ~/.kube/'
            sh 'echo "$KUBECONFIG_CONFIG" > ~/.kube/config'
            sh 'envsubst < deploy/ruoyi-gateway/deploy.yml | kubectl apply -f -'
          }

        }

      }
    }

  }
  environment {
    DOCKER_CREDENTIAL_ID = 'harbor'
    GITHUB_CREDENTIAL_ID = 'gitee'
    KUBECONFIG_CREDENTIAL_ID = 'k8s-1718281828'
    REGISTRY = 'core.harbor.1718281828.com'
    DOCKERHUB_NAMESPACE = 'ruoyi-cloud'
    APP_NAME = 'ruoyi-gateway'
  }
  parameters {
    string(name: 'TAG_NAME', defaultValue: '', description: '')
  }
}
